import numpy as np
import cv2
import os
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.optimizers import Adam

input_shape = (33, 28)
folder = 'Images/osu_nummers'

def load_images_from_folder(folder):
    images = []
    expected_output = np.zeros((10, 10))
    files = [name for name in os.listdir(folder) if os.path.isfile(os.path.join(folder, name))]
    for i, file in enumerate(files):
        image = cv2.imread(folder + '/{}'.format(file))
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        images.append(image)
        expected_output[i][i] = 1
    return np.array(images), expected_output

def create_model():
    model = Sequential()
    model.add(Dense(924, activation='relu', input_shape=input_shape))
    model.add(Flatten())
    model.add(Dense(10, activation='softmax'))
    model.compile(optimizer=Adam(), loss='categorical_crossentropy')
    return model

def fit(X, y):
    model.fit(X, y, epochs=15)
    model.save('network_weights/{}'.format('number_recognition_weights.h5'))

def test(X):
    model.load_weights('network_weights/number_recognition_weights.h5')
    number_image = np.reshape(X, (1, 33, 28))
    prediction = model.predict(number_image)
    if 1 in prediction[0]:
        return np.argmax(prediction[0])
    return None


model = create_model()
# X, y = load_images_from_folder(folder)