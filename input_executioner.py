import keyboard
import win32api
import win32con
import pyautogui

keys_pressed_down = []
pyautogui.FAILSAFE = False


def move_mouse(x, y):
    pyautogui.moveTo(x, y, 0.1, pyautogui.easeInElastic)


def click_mouse_down():
    pyautogui.mouseDown()


def click_mouse_up():
    pyautogui.mouseUp()


def set_key_events():
    add_key_events("q")


def add_key_to_list(key):
    if key not in keys_pressed_down:
        keys_pressed_down.append(key)


def get_keys_pressed_down():
    return keys_pressed_down


def add_key_events(key):
    keyboard.on_press_key(key, lambda _: add_key_to_list(key))
