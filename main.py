import input_executioner
import number_recognizer
import network
import cv2
import numpy as np
import pyautogui
import time
import win32gui
import copy
import csv
import os

fps = 30

template_image = cv2.imread('Images/Osu_online_ranking.png')
comma_image = cv2.imread('Images/comma.png')
comma_image = cv2.cvtColor(comma_image, cv2.COLOR_RGB2GRAY)
has_ended = False
global_highest_score = 1.06
global_games_played = 80

#gets the position of the window that's currently in focus
# @return window_rect: A tuple containing the x,y,width and height of the window
def get_window_position():
    window = win32gui.GetForegroundWindow()
    window_rect = win32gui.GetWindowRect(window)
    win32gui.SetForegroundWindow(window)
    return window_rect


def start_recording(model):
    global has_ended
    games_played = 0
    scores = []

    input_executioner.set_key_events()
    frame_amount = 0
    frame_timestamp = time.time()

    #cv2.namedWindow("Screen")
    #cv2.moveWindow("Screen", -960, 0)
    while games_played < 1:
        if time.time() - frame_timestamp >= 1 / fps or frame_amount == 0:
            frame, frame_timestamp = take_screenshot()
            frame_amount += 1
            if 'q' in input_executioner.get_keys_pressed_down():
                return None, None
            compressed_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            compressed_frame = cv2.resize(compressed_frame, (128, 72), interpolation=cv2.INTER_AREA)
            compressed_frame = np.array(compressed_frame)
            compressed_frame = np.reshape(compressed_frame, (1, 128, 72, 1))
            if has_ended is False:
                prediction = model.predict(compressed_frame)
                mouseX = (int)(prediction[0][0] * 1920)
                mouseY = (int)(prediction[0][1] * 1080)
                input_executioner.move_mouse(mouseX, mouseY)
                if prediction[0][2] >= 0.4:
                    input_executioner.click_mouse_down()
                else:
                    input_executioner.click_mouse_up()
            #cv2.imshow("Screen", compressed_frame)
            elif has_ended:
                accuracy = get_game_accuracy(frame)
                print(accuracy)
                scores.append(accuracy)
                games_played += 1
                input_executioner.move_mouse(1600, 810)
                input_executioner.click_mouse_down()
                input_executioner.click_mouse_up()
                has_ended = False
                break
            level_end_check(frame)
    score_index = np.argmax(scores)
    highest_score = scores[score_index]
    return highest_score


def take_screenshot():
    frame_timestamp = time.time()
    window = get_window_position()
    img = pyautogui.screenshot(region=(window[0], window[1], window[2] - window[0], window[3] - window[1]))
    frame = np.array(img)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    return frame, frame_timestamp


def level_end_check(frame):
    global has_ended
    result = cv2.matchTemplate(frame, template_image, cv2.TM_CCOEFF_NORMED)
    threshold = 0.85
    loc = np.where(result >= threshold)
    if len(loc[0] > 0):
        print('End of the level reached')
        input_executioner.click_mouse_up()
        time.sleep(5)
        has_ended = True


def get_game_accuracy(frame):
    accuracy_frame = frame[755:800, 435:615]
    accuracy_frame = cv2.cvtColor(accuracy_frame, cv2.COLOR_RGB2GRAY)
    cv2.imwrite('cool.png', accuracy_frame)
    result100 = cv2.matchTemplate(accuracy_frame[27:35, 86:94], comma_image, cv2.TM_CCOEFF_NORMED)
    result10 = cv2.matchTemplate(accuracy_frame[27:35, 59:67], comma_image, cv2.TM_CCOEFF_NORMED)
    result1 = cv2.matchTemplate(accuracy_frame[27:35, 32:40], comma_image, cv2.TM_CCOEFF_NORMED)
    if len(np.where(result100 > 0.85)[0]) > 0:
        return 100.00
    elif len(np.where(result10 > 0.85)[0]) > 0:
        first_number = accuracy_frame[3:36, 3:31]
        second_number = accuracy_frame[3:36, 30:58]
        third_number = accuracy_frame[3:36, 69:97]
        fourth_number = accuracy_frame[3:36, 96:124]
        first = number_recognizer.test(first_number)
        second = number_recognizer.test(second_number)
        third = number_recognizer.test(third_number)
        fourth = number_recognizer.test(fourth_number)
        return first * 10 + second + third/10 + fourth/100
    elif len(np.where(result1 > 0.85)[0]) > 0:
        first_number = accuracy_frame[3:36, 3:31]
        second_number = accuracy_frame[3:36, 42:70]
        third_number = accuracy_frame[3:36, 69:97]
        first = number_recognizer.test(first_number)
        second = number_recognizer.test(second_number)
        third = number_recognizer.test(third_number)
        return first + second/10 + third/100
        pass
    else:
        return None

#pas deze handmatig aan wanneer je hem aan zet
current_model = '1.06-80'
LEARNING_RATE = 0.0003
#model = network.create_model()
#best_model = model
best_model = network.load_weights(current_model)

time.sleep(1)

while True:
    new_model = network.mutate_weights(best_model, LEARNING_RATE)
    #new_model = model
    highest_score = start_recording(new_model)

    global_games_played += 1

    if highest_score is None:
        break

    if highest_score >= global_highest_score:
        global_highest_score = highest_score if highest_score > global_highest_score else global_highest_score
        network.save_weights(new_model, highest_score, global_games_played)
        print('Saved new model to file')
        best_model = new_model
    time.sleep(2)
