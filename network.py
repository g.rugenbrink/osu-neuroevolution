import numpy as np
from keras.models import Sequential, load_model
from keras.layers import Dense, Conv2D, Dropout, MaxPool2D, Flatten
from keras.optimizers import Adam


input_shape = (128, 72, 1)

def create_model():
    model = Sequential()
    model.add(Conv2D(256, 3, activation='relu', input_shape=input_shape))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Conv2D(128, 3, activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(3, activation='sigmoid'))
    model.compile(optimizer=Adam())
    return model

def save_weights(model, highest_score= 0.0, games_played=0):
    model.save('network_weights/{}-{}.h5'.format(highest_score, games_played))

def load_weights(model_name):
    model = load_model("network_weights/{}.h5".format(model_name))
    return model

def mutate_weights(model, learning_rate):
    base_weights = model.get_weights()
    modifier_array = []
    for i, layer in enumerate(base_weights):
        modifier = np.random.uniform(-learning_rate, learning_rate, layer.shape)
        modifier_array.append(modifier)
    new_weights = np.array(base_weights) + np.array(modifier_array)
    model.set_weights(new_weights)
    return model
